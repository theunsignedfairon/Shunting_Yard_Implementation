import java.util.EmptyStackException;
import java.util.Iterator;


/**
 * It is implementation of Single Linked List
 * base on Node implementation
 * @param <T>
 */
public class SingleLinkedList<T> implements Iterable<T> {
    private int size = 0; // Size of List
    private Node _head = null; // Reference to 1st element - head
    private Node _tail = null; // Reference to last element - tail


    /**
     * Implementation of iterator
     * @return
     */
    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Node<T> current = new Node<T>(null, null, _head);

            /**
             * Check existence of next element
             * @return
             */
            @Override
            public boolean hasNext() {
                return current.getNext() != null;
            }

            /**
             * Return next element
             * @return
             */
            @Override
            public T next() {
                if (!hasNext()) throw new IndexOutOfBoundsException();
                current = current.getNext();
                return current.getValue();
            }
        };
    }

    /**
     * Method checks List on empty
     *
     * @return
     */
    public boolean isEmpty() {
        return (size == 0);
    }


    /**
     * Add element in the end of List (set new tail)
     *
     * @param value
     */
    public void addLast(T value) {
        Node<T> node = new Node<T>(value);
        // Special case when list is empty
        if (isEmpty()) {
            node.setNext(null);
            _head = node;
            _tail = node;
            // Default case
        } else {
            _tail.setNext(node);
            _tail = node;
            _tail.setNext(null);
        }
        size++;
    }


    /**
     * Add element in the beginning of List (set new head)
     *
     * @param value
     */
    public void addFirst(T value) {
        Node<T> node = new Node<T>(value);
        // Special case when List is empty
        if (isEmpty()) {
            node.setNext(null);
            _head = node;
            _tail = node;
            // Default case
        } else {
            node.setNext(_head);
            _head = node;
        }
        size++;
    }


    /**
     * Add element by index
     *
     * @param index
     * @param value
     */
    public void add(int index, T value) {
        // Exception when index out of range or List is empty
        if ((index < 0 || index >= getSize()) && (!isEmpty())) {
            throw new IndexOutOfBoundsException();
        } else {
            // Special case when we add in the beginning
            if (index == 0) {
                addFirst(value);
                // Special case when we add in the last
            } else if (index == getSize() - 1) {
                addLast(value);
                // Default case
            } else {
                Node<T> curr = _head;
                Node<T> node = new Node<T>(value);
                for (int i = 1; i <= index - 1; i++) {
                    curr = curr.getNext();
                }
                node.setNext(curr.getNext());
                curr.setNext(node);
                size++;
            }
        }
    }


    /**
     * Method return size of List
     *
     * @return
     */
    public int getSize() {
        return size;
    }


    /**
     * Method gets element by index
     *
     * @param index
     * @return
     */
    public T getByIndex(int index) {
        // Exception when index out of range or List is empty
        if (isEmpty() || index >= getSize() || index < 0) {
            throw new IndexOutOfBoundsException();
        } else {
            Node<T> curr = _head;
            for (int i = 1; i <= index; i++) {
                curr = curr.getNext();
            }
            return curr.getValue();
        }
    }


    /**
     * Method set value of element (Upgrade value of element)
     *
     * @param index
     * @param value
     */
    public void setByIndex(int index, T value) {
        // Exception when index out of range or List is empty
        if (isEmpty() || index >= getSize() || index < 0) {
            throw new IndexOutOfBoundsException();
            // Default case
        } else {
            Node<T> curr = _head;
            for (int i = 0; i < index; i++) {
                curr = curr.getNext();
            }
            curr.setValue(value);
        }
    }


    /**
     * Method removes and returns 1st element
     *
     * @return
     */
    public T removeFirst() {
        // Exception when List is empty
        if (isEmpty()) {
            throw new EmptyStackException();
            // Case when out List has only one element
        } else if (getSize() == 1) {
            Node<T> res = _head;
            _head = null;
            _tail = null;
            size--;
            return res.getValue();
            // Default case
        } else {
            Node<T> res = _head;
            _head = _head.getNext();
            size--;
            return res.getValue();
        }
    }


    /**
     * Method removes and returns last element;
     *
     * @return
     */
    public T removeLast() {
        // Exception in case List is empty
        if (isEmpty()) {
            throw new EmptyStackException();
            // Special case when List contains just one element
        } else if (getSize() == 1) {
            Node<T> res = _tail;
            _tail = null;
            _head = null;
            size--;
            return res.getValue();
        } else {
            Node<T> res = _tail, curr = _head;
            for (int i = 1; i < getSize() - 1; i++) {
                curr = curr.getNext();
            }
            _tail = curr;
            _tail.setNext(null);
            size--;
            return res.getValue();
        }
    }


    /**
     * Method removes element by index
     *
     * @param index
     * @return
     */
    public T removeByIndex(int index) {
        // Exception when index out of range or List is empty
        if (isEmpty() || index < 0 || index >= getSize()) {
            throw new IndexOutOfBoundsException();
            // Special case when List contains just one element
        } else if (getSize() == 1) {
            Node<T> buf = _head;
            _head = null;
            _tail = null;
            size--;
            return buf.getValue();
            // Default case
        } else {
            Node<T> curr = _head, res;
            for (int i = 1; i <= index - 1; i++) {
                curr = curr.getNext();
            }
            res = curr.getNext();
            curr.setNext(curr.getNext().getNext());
            size--;
            return res.getValue();
        }
    }


    /**
     * Method prints all elements of List
     */
    public void printList() {
        Iterator<T> iterator = iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.print("\n");
    }
}