public class ReversePolishNotation {
    /**
     * Method determines is symbol separator or not?
     * @param c
     * @return
     */
    static private boolean isSeparator(char c) {
        if (" =".indexOf(c) != -1) {
            return true;
        }
        return false;
    }


    /**
     * Method determines is symbol operator or not?
     * @param c
     * @return
     */
    static private boolean isOperator(char c) {
        if ("+-/*()".indexOf(c) != -1) {
            return true;
        }
        return false;
    }


    /**
     * Method returns priority of operation
     * @param c
     * @return
     */
    static private byte getPriority(char c) {
        // Switch operator to get priority
        switch (c) {
            case ')': return 0;
            case '(': return 1;
            case '+': return 2;
            case '-': return 3;
            case '*': return 4;
            case '/': return 4;
            case '^': return 5;
            default: return 6;
        }
    }


    /**
     *
     * @param input_string
     * @return
     */
    static public double counting(String input_string) {
        // Declaration
        double result = 0; // Store result of expression
        LinkedStack<Double> stack_for_current_actions = new LinkedStack<Double>();  // Stack stores current operations
        for (int index = 0; index < input_string.length(); index++) {
            // If current symbol is Digit then we scan next number
            if (Character.isDigit(input_string.charAt(index))) {
                // Scanned number
                String number = new String();
                // While current symbol is not Separator continue
                while (!isOperator(input_string.charAt(index)) && !isSeparator(input_string.charAt(index))) {
                    number += input_string.charAt(index);
                    index++;
                    // If it is the end of string then break cycle
                    if (index == input_string.length()) break;
                }
                // Push number in stack
                stack_for_current_actions.push(Double.parseDouble(number));
                index--;
            // If current symbol is operator
            } else if (isOperator(input_string.charAt(index))) {
                // Pop from stack two numbers
                double a = stack_for_current_actions.pop(), b = stack_for_current_actions.pop();
                // Arithmetic operations
                switch (input_string.charAt(index)) {
                    case '+':
                        result = b + a;
                        break;
                    case '-':
                        result = b - a;
                        break;
                    case '*':
                        result = b * a;
                        break;
                    case '/':
                        result = b / a;
                        break;
                }
                // Push result in stack
                stack_for_current_actions.push(result);
            }
        }

        // Return total result
        return stack_for_current_actions.peek();
    }


    /**
     * Method returns expression in Reverse Polish Notation
     * @param input_string
     * @return
     */
    static private String getExpression(String input_string) {
        String output_string = new String();
        LinkedStack<Character> operation_stack = new LinkedStack<Character>();

        for (int index = 0; index < input_string.length(); index++) {
            // If current symbol is Separator then continue cycle
            if (isSeparator(input_string.charAt(index))) {
                continue;
            }
            // If current symbol is Digit, then we scan next number
            if (Character.isDigit(input_string.charAt(index))) {
                // Scan until current symbol is not Separator or Operator
                while (!isSeparator(input_string.charAt(index)) && !isOperator(input_string.charAt(index))) {
                    output_string += input_string.charAt(index);
                    index++;
                    if (index == input_string.length()) break; // If current symbol is last, then we break cycle
                }
                output_string += " ";
                index--; // Return to previous symbol
            }
            // If current symbol is Operator
            if (isOperator(input_string.charAt(index))) {
                // If current symbol is open bracket
                if (input_string.charAt(index) == '(') {
                    operation_stack.push(input_string.charAt(index));
                // If current symbol is close bracket
                } else if (input_string.charAt(index) == ')') {
                    // We pop all symbols from stack until we find open bracket
                    char c = operation_stack.pop();
                    while (c != '(') {
                        output_string += c + " ";
                        c = operation_stack.pop();
                    }
                // If current symbol is other operators
                }  else {
                    // If Stack is not empty
                    if (operation_stack.getSize() > 0)
                        // If priority of current symbol less then priority of symbol from top of stack
                        // we add in string element from top of stack and pop it from stack
                        if (getPriority(input_string.charAt(index)) <= getPriority(operation_stack.peek())) {
                            output_string += operation_stack.pop() + " ";
                        }
                    // Push Operator in stack
                    operation_stack.push(input_string.charAt(index));
                }
            }
        }
        // Pop from stack all elements which are left
        while (operation_stack.getSize() > 0) {
            output_string += operation_stack.pop() + " ";
        }
        // Return result - expression in Reverse Polish Notation
        return output_string;
    }


    /**
     * Method whick allows to have access to class. Main method
     * @param input_string
     * @return
     */
    public static double calculate(String input_string) {
        return counting(getExpression(input_string));
    }
}
